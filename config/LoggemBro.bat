PUSHD %~dp0
SET DIR=%CD%
POPD

set PATH=%DIR%/lib;%PATH%
set PATH=%DIR%/qt/bin;%PATH%

start "LoggemBro" "%DIR%\bin\LoggemBro.exe"
exit
