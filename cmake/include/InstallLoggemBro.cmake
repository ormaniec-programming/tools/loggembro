if ( WIN32 )
    if ( CMAKE_BUILD_TYPE STREQUAL "Release" )
        set_property( TARGET main PROPERTY WIN32_EXECUTABLE true )
    endif ()

    set( SHARED_LIB_REGEX ".*\\.dll|.*\\.a" )
    set( STARTUP_SCRIPT_EXTENSION ".bat" )
    set( LIBRARY_DIR "bin" )
elseif ( UNIX )
    set( SHARED_LIB_REGEX ".*\\.so.*" )
    set( STARTUP_SCRIPT_EXTENSION ".sh" )
    set( LIBRARY_DIR "lib" )
endif ()

install(
    TARGETS ${EXEC_NAME}
    RUNTIME DESTINATION bin/
    LIBRARY DESTINATION lib/
)
#install( FILES ${CMAKE_SOURCE_DIR}/config/qt.conf DESTINATION ./)
install( FILES "${CMAKE_SOURCE_DIR}/config/LoggemBro${STARTUP_SCRIPT_EXTENSION}" DESTINATION ./ )
install( DIRECTORY "${QT6_DIR}/plugins" DESTINATION qt/
    FILES_MATCHING REGEX "${SHARED_LIB_REGEX}"
    )
install( DIRECTORY "${QT6_DIR}/${LIBRARY_DIR}" DESTINATION qt/
    FILES_MATCHING REGEX "${SHARED_LIB_REGEX}"
    PATTERN "cmake" EXCLUDE
    PATTERN "metatypes" EXCLUDE
    )
if ( UNIX )
    install( CODE [[
      file(GET_RUNTIME_DEPENDENCIES
          EXECUTABLES $<TARGET_FILE:LoggemBro>
          RESOLVED_DEPENDENCIES_VAR _r_deps
          UNRESOLVED_DEPENDENCIES_VAR _u_deps
      )
      message( STATUS "UNRESOLVED: ${_u_deps}" )
      foreach(_file ${_r_deps})
          file(INSTALL
              DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
              TYPE SHARED_LIBRARY
              FOLLOW_SYMLINK_CHAIN
              FILES "${_file}"
          )
      endforeach()
      list(LENGTH _u_deps _u_length)
      if("${_u_length}" GREATER 0)
          message(STATUS "Unresolved dependencies detected!")
      endif()

      file(GLOB FILES_FOUND "${CMAKE_INSTALL_PREFIX}/lib/*" )
      file(GLOB FILES_FOUND_QT "${CMAKE_INSTALL_PREFIX}/qt/lib/*" )

      set(EXTRACTED_BASE)
      set(EXTRACTED_QT_BASE)
      set(COLLIDED_BASE)
      foreach( _file ${FILES_FOUND} )
        get_filename_component(out "${_file}" NAME)
        LIST(APPEND EXTRACTED_BASE "${out}")
      endforeach()
          foreach( _file ${FILES_FOUND_QT} )
        get_filename_component(out "${_file}" NAME)
        LIST(APPEND EXTRACTED_QT_BASE "${out}")
      endforeach()

      foreach( lib ${EXTRACTED_BASE} )
          list( FIND EXTRACTED_QT_BASE "${lib}" _index )
          if( ${_index} GREATER -1 )
              list( APPEND COLLIDED_BASE "${lib}" )
          endif()
      endforeach()
      list( TRANSFORM COLLIDED_BASE PREPEND ${CMAKE_INSTALL_PREFIX}/lib/ )
      file( REMOVE ${COLLIDED_BASE} )
    ]] )
endif ()